# PyInvaders
Python port of Space Invaders™ built on PyGame with "lite" OOD structure from ~500loc and ~50 settings.


## Requirements
You must have the following packages installed for PyInvaders to run:
- PipEnv
- PyEnv (optional, but recommended for directory-specific Python versioning)


## Getting Started
1. Download the project
	- `git clone https://gitlab.com/robr3rd/pyinvaders.git`
1. Enter the project folder
	- `cd pyinvaders`
1. Install project dependencies
	- `pipenv install`
1. Run the game!
	- `./bin/pyinvaders`


## Extra
To squeeze even more enjoyment out of PyInvaders, take a look inside `settings.yml` and customize to your heart's desire! Every aspect of the game is tweakable!


## Roadmap
- Actual FPS (dynamic movement speed based on FPS rate)
- Variable display size (settings)
- Variable display size (drag-and-drop)
- Audio
- Main menu
- High scores
