#! /usr/bin/env python
"""PyInvaders"""

import time
import random
import yaml
import pygame
# from pprint import pprint


# Helpers
def is_offscreen(rect):
    """Detect if a rect object is currently beyond the game's display."""
    center_x = rect.x + rect.width/2  # center of the rect (x-axis)
    center_y = rect.y + rect.height/2  # center of the rect (y-axis)

    if (center_x <= 0 or                         # left
        center_y <= 0 or                         # top
        center_x >= SETTINGS['window_width'] or  # right
        center_y >= SETTINGS['window_height']):  # bottom
        return True
    return False


# Controls
def controls(entity):
    """Parse input on behalf of an entity.  Can also exit the game."""
    for event in pygame.event.get():
        if event.type == pygame.QUIT:  # window closed
            GAME.quit()
        if event.type is pygame.KEYDOWN:
            if event.key in [pygame.K_ESCAPE, pygame.K_q]:
                GAME.quit()
            if event.key is pygame.K_SPACE:  # 'attack' in 'KEYDOWN' = 1 attack per press
                entity.attack()

    # Enable multi-key usage (more than a single key at once, e.g. firing AND moving
    keys = pygame.key.get_pressed()  # obtain the state of ALL keys on the keyboard

    if keys[pygame.K_RIGHT] and keys[pygame.K_LEFT]:  # both = no movement
        entity.move()
    elif keys[pygame.K_RIGHT]:
        entity.move('right')
    elif keys[pygame.K_LEFT]:
        entity.move('left')
    else:
        entity.move()

    if SETTINGS['player_missile_autofire']:  # hold down space for 1 attack per frame
        if keys[pygame.K_SPACE]:
            entity.attack()


# Models
class Projectile(pygame.sprite.Sprite):
    """Generic projectile"""
    def __init__(self, source, pos, image, speed):
        pos_x, pos_y = pos

        super().__init__()

        if not isinstance(source, object):
            raise ValueError('"source" must be an object', source)
        self.source = source  # the object that launched this projectile

        self.exploded_at = None
        self.add(ALL_SPRITES)  # add to global convenience group of all Sprites

        # Images
        if isinstance(image, dict):
            self.images = {
                'base': pygame.image.load(image['base']).convert_alpha(),
                'explosion': pygame.image.load(image['explosion']).convert_alpha(),
                'scale': image.get('scale', 1)
            }
        elif isinstance(image, str):
            self.images = {
                'base': pygame.image.load(image).convert_alpha(),
                'explosion': pygame.image.load(SETTINGS['explosion_sprite_path']).convert_alpha(),
                'scale': 1
            }
        else:
            raise ValueError('Argument "image" must be of type "dict" or "str".', image)

        self.image = self.images['base']

        size_new_w = int(self.image.get_rect().width * self.images['scale'])
        size_new_h = int(self.image.get_rect().height * self.images['scale'])
        self.image = pygame.transform.scale(self.image, (size_new_w, size_new_h))

        self.rect = self.image.get_rect()
        self.rect.x = int(pos_x-self.rect.width/2)
        self.rect.y = int(pos_y)

        # Speed
        if isinstance(speed, dict):
            self.speed = speed
        elif isinstance(speed, int):  # assume vertical since it's most likely
            self.speed = {
                'x': 0,
                'y': speed
            }
        else:
            raise ValueError('Invalid value given for "speed".  Must be dict or str.', speed)

    def update(self):
        """tick - Move, Remove, and Explode this projectile as necessary"""
        # Clean up destroyed projectiles after enough time to see they're destroyed
        if self.exploded_at is not None:
            if not SETTINGS['piercing_missiles']:  # pierce = move after explosion
                self.speed['x'] *= SETTINGS['explosion_slow']
                self.speed['y'] *= SETTINGS['explosion_slow']
            if not SETTINGS['debris'] and not SETTINGS['piercing_missiles']:  # debris = never clean up
                exploded_for = time.time() - self.exploded_at
                if exploded_for >= SETTINGS['explosion_time']:
                    self.kill()
                    return self

        # Move this projectile
        self.rect.x += int(self.speed['x'])
        self.rect.y += int(self.speed['y'])

        # Clean up MISSED projectiles that leave the screen
        if is_offscreen(self.rect):
            self.kill()

    def explode(self):
        """boom"""
        self.image = self.images['explosion']
        self.exploded_at = time.time()


class ProjectileFactory(pygame.sprite.Sprite):
    """Creator and manager of all projectiles"""


class Ship(pygame.sprite.Sprite):
    """Shared attributes and actions for all ships"""
    def __init__(self):
        self.rect = None
        self.projectile = None

        super().__init__()

        self.exploded_at = None  # track when explosion started
        self.speed = {
            'x': 0,
            'y': 0,
            'x_direction': 0,
            'y_direction': 0
        }
        self.images = {
            'base': pygame.image.load(SETTINGS['enemy_sprite_path']),
            'explosion': pygame.image.load(SETTINGS['explosion_sprite_path'])
        }
        self.image = self.images['base']
        self.add(ALL_SPRITES)

    def update(self):
        """tick"""
        if self.exploded_at is not None:  # halt exploded projectiles
            self.speed['x'] *= SETTINGS['explosion_slow']
            self.speed['y'] *= SETTINGS['explosion_slow']
        self.rect.x += int(self.speed['x'] * self.speed['x_direction'])
        self.rect.y += int(self.speed['y'] * self.speed['y_direction'])

    def attack(self):
        """Fire a projectile"""
        projectile = Projectile(
            self,
            (self.rect.x+self.rect.width/2, self.rect.y-self.rect.height/2),
            self.projectile['sprites'],
            self.projectile['speed']
        )
        PROJECTILES.add(projectile)

    def explode(self):
        """boom"""
        self.image = self.images['explosion']
        self.exploded_at = time.time()


class Player(Ship):
    """The player ship"""
    def __init__(self):
        super().__init__()

        # Settings and images of projectile
        self.projectile = {
            'speed': SETTINGS['player_missile_speed'] * -1,  # go up, not down
            'sprites': {
                'base': SETTINGS['player_missile_sprite_path'],
                'explosion': SETTINGS['explosion_sprite_path'],
                'scale': SETTINGS['player_missile_scale']
            }
        }

        # Images of ship
        self.images = {
            'base': pygame.image.load(SETTINGS['player_sprite_path']),
            'explosion': pygame.image.load(SETTINGS['explosion_sprite_path']),
            'move': {
                'left': pygame.image.load(SETTINGS['player_left_sprite_path']),
                'right': pygame.image.load(SETTINGS['player_right_sprite_path'])
            }
        }
        self.image = self.images['base']
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = (0, SETTINGS['window_height'] - self.rect.height)

        # Speed
        self.speed['x'] = SETTINGS['player_speed_x']
        self.speed['y'] = SETTINGS['player_speed_y']

    def update(self):
        """tick"""
        controls(self)
        return super().update()

    def move(self, direction=None):
        """Move in the given direction"""
        if direction == 'left':
            if self.rect.x <= 0:
                self.speed['x_direction'] = 0
            else:
                self.speed['x_direction'] = -1  # negative = 'left'
        elif direction == 'right':
            if (self.rect.x+self.rect.width) >= SETTINGS['window_width']:
                self.speed['x_direction'] = 0
            else:
                self.speed['x_direction'] = 1
        elif direction is None:
            self.speed['x_direction'] = 0
        elif direction is not None:
            raise ValueError('Invalid "direction" provided.  Must be "left", "right", or None.', direction)

        # Image
        if direction is None:  # restore 'base' image
            self.image = self.images['base']
        elif direction in self.images['move']:  # if one for this direction was supplied
            self.image = self.images['move'][direction]


class Enemy(Ship):
    """An enemy ship"""
    def __init__(self):
        super().__init__()

        # Settings and images of projectile
        self.projectile = {
            'speed': SETTINGS['enemy_missile_speed'],
            'sprites': {
                'base': SETTINGS['enemy_missile_sprite_path'],
                'explosion': SETTINGS['explosion_sprite_path'],
                'scale': SETTINGS['enemy_missile_scale']
            }
        }

        # Images of ship
        self.images = {
            'base': pygame.image.load(SETTINGS['enemy_sprite_path']),
            'explosion': pygame.image.load(SETTINGS['explosion_sprite_path'])
        }
        self.image = self.images['base']
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = (self.rect.width, 0)

        # Speed
        self.speed['x'] = SETTINGS['enemy_speed_x']
        self.speed['y'] = SETTINGS['enemy_speed_y']
        self.speed['x_direction'] = 1

    def update(self):
        """tick - Move or Remove this ship"""
        # Clean up destroyed enemies after enough time to see they're destroyed
        if self.exploded_at is not None and not SETTINGS['debris']:  # debris = never clean up
            exploded_for = time.time() - self.exploded_at
            if exploded_for >= SETTINGS['explosion_time']:
                self.kill()

        # Change direction at edge of screen
        if self.rect.x < 0 or (self.rect.x+self.rect.width) >= SETTINGS['window_width']:
            self.speed['x_direction'] *= -1
            self.speed['y_direction'] = 1
        else:
            self.speed['y_direction'] = 0

        # Increase movement speed based on armada (squadron_qty*squadron_size) health.  (fewer ships = faster ships)
        self.speed['x'] = SETTINGS['enemy_speed_x'] + (SETTINGS['squadrons'] * SETTINGS['squadron_size'] - len(GAME.armada.sprites())) / SETTINGS['armada_adrenaline_reducer']

        return super().update()


class Armada(pygame.sprite.Group):
    """
    Collection of ships

    Basically, it's a 'ShipFactory' or 'EnemyManager'
    """
    def __init__(self):
        super().__init__()
        self.squadrons = {}  # contains Sprite Groups
        self.generate()

    def generate(self):
        """Generate initial fleet of enemy ships"""
        for squadron in range(SETTINGS['squadrons']):
            self.squadrons[squadron] = pygame.sprite.Group()

            for ship in range(SETTINGS['squadron_size']):
                enemy = Enemy()
                enemy.rect.x = int(enemy.rect.width*1.5) * ship
                enemy.rect.y = enemy.rect.height*2 * squadron
                enemy.add(self, self.squadrons[squadron])

    def update(self):
        """tick"""
        super().update()
        # 1 ship remaining can't fire for a whole armada, so slowly step down the firing frequency
        if random.randint(1, 100) <= SETTINGS['armada_attack_chance']:
            random.choice(self.sprites()).attack()  # tell a random ship to fire


class Score:
    """Keep track of score"""
    def __init__(self, max_score):
        self.font_size = SETTINGS['score_font_size']
        self.current = SETTINGS['score_start']
        self.total = max_score

    def check_win(self):
        """Check for win"""
        if self.current >= self.total:
            return True
        return False

    def increment(self):
        """Increment the score"""
        self.current += SETTINGS['score_per_enemy']
        return self

    def render(self):
        """Display the score"""
        SCREEN.blit(
            pygame.font.Font(None, self.font_size).render(
                f'Score: {self.current} / {self.total}',
                True,
                (255, 255, 255)
            ), (5, 5)  # x,y coordinates of the text
        )


class Game:
    """Main loop"""
    def __init__(self):
        self.armada = Armada()
        self.player = Player()
        self.score = Score(SETTINGS['squadrons'] * SETTINGS['squadron_size'])

    def start(self):
        """Start the game"""
        self.update()

    def update(self):
        """Process a single game tick"""
        clock = pygame.time.Clock()  # basically for consistent FPS

        while True:
            SCREEN.blit(BACKDROP, (0, 0))  # re-draw backdrop, which hides old artifacts

            self.player.update()
            self.armada.update()
            PROJECTILES.update()

            self.collisions()

            self.score.render()
            ALL_SPRITES.draw(SCREEN)
            pygame.display.update()

            clock.tick(SETTINGS['game_fps'])  # govern game speed from being "CPU max"

    def collisions(self):
        """Checks EVERYTHING for collisions"""
        # Handle Projectiles colliding into...
        for projectile in PROJECTILES.sprites():
            if projectile.exploded_at is None:
                # Player projectiles
                if isinstance(projectile.source, Player):
                    # ...each other!
                    pcollisions = pygame.sprite.spritecollide(projectile, PROJECTILES, False)
                    for pcollision in pcollisions:  # projectile-to-projectile collisions
                        if isinstance(pcollision.source, Enemy):  # only impact opposite type
                            projectile.explode()
                            pcollision.explode()
                            continue  # can no longer kill anything

                    # ...Enemies!
                    enemieshit = pygame.sprite.spritecollide(projectile, self.armada, False)
                    for enemy in enemieshit:
                        projectile.explode()  # ship debris explodes projectile
                        if enemy.exploded_at is None:  # prevent "double dipping"
                            enemy.explode()
                            self.point()

                # Enemy projectiles
                if isinstance(projectile.source, Enemy):
                    # ...Players!
                    playerhits = pygame.sprite.spritecollide(self.player, PROJECTILES, False)
                    if playerhits:  # if any hits
                        for playerhit in playerhits:  # possible multi-hit on Player
                            playerhit.explode()
                        self.player.explode()
                        self.lose('You were shot down!')

        # Handle Player/Enemy collisions
        enemiesrammed = pygame.sprite.spritecollide(self.player, self.armada, False)
        if enemiesrammed:
            for enemy in enemiesrammed:
                enemy.explode()
                self.point()
            self.player.explode()
            self.lose('You were struck by an enemy ship!')

    def point(self):
        """Check for win condition"""
        self.score.increment().render()

        if not SETTINGS['overkill']:
            if self.score.check_win():
                self.win()
        elif SETTINGS['overkill'] and len(self.armada.sprites()) == 0:
            self.win('Victory through GENOCIDE!')

    def lose(self, message=None):
        """Handle game loss"""
        if not message:
            message = 'You lose!'
        self.quit(message)

    def win(self, message='Victory!'):
        """Handle game victory"""
        print(message)
        if SETTINGS['endless_mode']:
            self.armada = Armada()
        else:
            self.quit()

    def quit(self, message=None):
        """Close the game and print the final score"""
        if message:
            print(message)
        exit('Your score: ' + str(self.score.current) + '/' + str(self.score.total))


# Initialize
pygame.init()
pygame.display.set_caption('PyInvaders')  # set the window title

# Import Game Settings
SETTINGS_FILE = open('settings.yml')
SETTINGS = yaml.full_load(SETTINGS_FILE)

# Set screen (window) size and backdrop
SCREEN = pygame.display.set_mode((SETTINGS['window_width'], SETTINGS['window_height']))
BACKDROP = pygame.image.load(SETTINGS['backdrop_path']).convert()

# Sprite Groups
ALL_SPRITES = pygame.sprite.Group()
PROJECTILES = pygame.sprite.Group()

# Begin!
GAME = Game()
GAME.start()
